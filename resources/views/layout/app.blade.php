 <!DOCTYPE html>
 <html lang="ja">

 <head>
     <meta charset="utf-8">
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>@yield('title')</title>

     {{-- styles --}}
     <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
     <link rel="stylesheet" href="{{ asset('/css/header.css') }}">
     @yield('head')

     {{-- fontawesome --}}
     <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
     {{-- babel --}}
     <script src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.min.js"></script>
     {{-- jquery --}}
     <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
     {{-- popper --}}
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
         integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
     </script>
     {{-- bootstrap --}}
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
         integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
     <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
         integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
     </script>



     <script type="text/javascript" src="{{ asset('/js/t.min.js') }}"></script>
     <script type="text/javascript" src="{{ asset('/js/accordion.js') }}"></script>
     <script type="text/javascript" src="{{ asset('/js/typing.js') }}"></script>
 </head>

 <body class="container-fluid p-0 d-block d-md-flex">
     <div class="col-12 col-md-2 bg-blue nav-header__size">
         <nav class="d-md-block d-flex justify-content-around">
             <div class="nav__logo">
                 <img class="img-fluid" src="{{ asset('/img/headerlogo.png') }}">
             </div>
             <div class="nav__menu">
                 <div class="link__bottum d-block" id="menu">menu</div>
                 <ul class="nav__menu--ul">
                     <li><a class="red" href="/project/admin">admin article{{ csrf_field() }}</a></li>
                     <li><a class="red" href="/project/create">new article{{ csrf_field() }}</a></li>
                     <li><a class="yellow" href="/callendar">callendar</a></li>
                     <li><a class="yellow" href="/remainder" method="get">remainder</a></li>
                 </ul>
             </div>

             <ul class="accordion2" id="menu" style="padding-left: 0px;">
                 <li>
                     <p class="ac1 link__bottum">menu</p>
                     <ul class="inner">
                         <li class="content1-1">
                             <a class="red" href="/project/admin">admin article{{ csrf_field() }}</a>
                         </li>
                         <li class="content1-2">
                             <a class="red" href="/project/create">new article{{ csrf_field() }}</a>
                         </li>
                         <li class="content1-3">
                             <a class="yellow" href="/callendar">callendar</a>
                         </li>
                         <li class="content1-4">
                             <a class="yellow" href="/remainder" method="get">remainder</a>
                         </li>
                     </ul>
                 </li>
             </ul>
             <a href="/contact" method="get">
                 <div class="link__bottum">contact</div>
             </a>
             <a href="/">
                 <div class="link__bottum">{{ csrf_field() }}Log in/out</div>
             </a>
             <a href="/test" method="get">
                 <div class="link__bottum">test</div>
             </a>
             {{-- ハンバーガーメニュー --}}
             <div id="bargar">
                 <span class="bargar__line bargar__line--1"></span>
                 <span class="bargar__line bargar__line--2"></span>
                 <span class="bargar__line bargar__line--3"></span>
             </div>
             <div class="black-bg" id="js-black-bg">
                 <div class="global-nav">
                     <ul class="global-nav__list">
                         <li class="global-nav__item"><a href="">メニュー1</a></li>
                         <li class="global-nav__item"><a href="">メニュー2</a></li>
                         <li class="global-nav__item"><a href="">メニュー3</a></li>
                         <li class="global-nav__item"><a href="">メニュー4</a></li>
                         <li class="global-nav__item"><a href="">メニュー5</a></li>
                     </ul>
                 </div>
             </div>

         </nav>
     </div>

     <div class="col-12 col-md-9">
         <div id="js-target">
             <p style="color: aliceblue;!important">
                 <dl style="display:inline">paxcreation</dl> workshare service
             </p>
         </div>
         <div class="mt-0 content__wrapp">
             @yield('content')
         </div>
     </div>

     @yield('script')
     <script type="text/javascript" src="{{ asset('/js/bagar.js') }}"></script>
     <script src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.2/moment.min.js"></script>
     <script src="https://cdn.jsdelivr.net/npm/vue"></script>


 </body>

 </html>
