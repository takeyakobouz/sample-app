@extends('layout.app')

@section('title', 'Callendar')

@section('head')
@endsection

@section('content')

<body>
    <div style="padding:10px; padding-left:20px">
        <div>
            <div class="d-flex">
                <div style="font-size:20px">
                    <h1 style="font-size:40px;">{{ $month }}</h1>
                </div>
                <div>
                    <div class="ml-3">
                        <p style="margin:0px;" class="mt-2">{{ $monthEng }}</p>
                    </div>
                    <div class="ml-3">
                        <p style="margin:0px;">{{ $year }}</p>
                    </div>
                </div>
                <div class="ml-3">
                    <div id="bug" class="bugBox">
                        <i class="fas fa-bug awesomeStyle"></i>
                    </div>
                </div>
                <div id="jsCallendar"></div>
            </div>
            <div class="d-flex link">
                <div class="border-t"><a href="/callendar/prev">&laquo;</a></div>
                <div colspan="5"><a href="/callendar">=</a></div>
                <div class="border-t"><a href="/callendar/next">&raquo;</a></div>
            </div>
        </div>
        <table class="table table-bordered" style="margin-top:10px; min-height:70vh;">
            <thead>
                <tr class="text-center">
                    @foreach ($week as $dayOfWeek)
                    <th>{{ $dayOfWeek }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach ($nowdate as $date)
                @if ($date->dayOfWeek === 0)
                <tr>
                    @endif
                    <td>
                        <a href= />
                        <p @if ($date->copy()->isCurrentMonth())
                            @if($date->copy()->isSunday())
                            style="color:red;min-width: 80px;"
                            @elseif($date->copy()->isSaturday())
                            style="color:blue;min-width: 80px;"
                            @else
                            style="color:black;min-width: 80px;"
                            @endif
                            @else
                            @if($date->copy()->isSunday())
                            style="color:red;background-color:#EFEFEF;min-width: 80px;"
                            @elseif($date->copy()->isSaturday())
                            style="color:blue;background-color:#EFEFEF;min-width: 80px;"
                            @else
                            style="color:black;background-color:#EFEFEF;min-width: 80px;"
                            @endif
                            @endif
                            >
                            {{ $date->day }}
                            {{-- {{ $date->dayOfWeek }} --}}
                            {{-- {{ $date->format('Y-m-d')}} --}}
                        </p>
                        </a>
                    </td>
                    @if ($date->dayOfWeek === 6)
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
    @endsection

    @section('script')
    <script src="{{ asset('/js/bug.js') }}"></script>
    <script src="{{ asset('/js/jsCallendar.js') }}"></script>
    @endsection
