@extends('layout.app')

@section('title', 'Project Admin')


@section('head')
@endsection

@section('content')
<div class="wrapp">
    <div class="admin__title">
        <h1>Admin</h1>
    </div>
    <div class="admin__contents">
        <table>
            <tbody>
                <tr class="admin__thead">
                    <th>管理</th>
                    <th>ID</th>
                    <th>案件</th>
                    <th>依頼主</th>
                    <th>業種</th>
                    <th>契約期間</th>
                    <th>勤務時間</th>
                    <th>職場</th>
                    <th>報酬</th>
                </tr>
                @forelse ($projects as $project)
                <tr class="admin-projects__item--basic">
                    <td class="d-flex justyfy-content-between">
                        <div>
                            <form action="{{url('/project/read',$project->id)}}" method="get">
                                {{ csrf_field() }}
                                <button class="linkbottom"><i class="fab fa-readme"></i></button>
                            </form>
                        </div>
                        <div>
                            <form action="{{ url('/project/edit', $project->id)}}" method="get">
                                {{ csrf_field() }}
                                <button class="linkbottom"><i class="fas fa-edit"></i></button>
                            </form>
                        </div>
                        <div>
                            <form action="/project/create" method="get">
                                {{ csrf_field() }}
                                <button class="linkbottom"><i class="far fa-sticky-note"></i></button>
                            </form>
                        </div>
                        <div>
                            <form action="{{url('/project/delete',$project->id)}}" method="post">
                                {{ csrf_field() }}
                                <button class="linkbottom"><i class="fas fa-trash-alt"></i></button>
                            </form>
                        </div>
                    </td>
                    <td>{{ $project->id }}</td>
                    <td>{{ $project->title }}</td>
                    <td>{{ $project->clients }}</td>
                    <td>{{ $project->categories }}</td>
                    <td>{{ $project->work_period }}</td>
                    <td>{{ $project->work_time }}</td>
                    <td>{{ $project->work_place }}</td>
                    <td>{{ $project->salary }}</td>
                </tr>
                @empty
                <tr>
                    <td>No project yet</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('script')
@endsection
