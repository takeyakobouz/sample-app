@extends('layout.app')

@section('title', 'Project Edit')

@section('head')
@endsection

@section('content')
<div class="admin_box mt-3" style="background-color:white; overflow:scroll;">
    <div class="text-center" style="max-width: 70vw; margin-left:90px;word-brake:brake-all">
        <p>TITLE<span style="font-size:40px; margin:0 15px;">{{ $project->title }}</span></p>
        <a href="/project/admin" method="get">
            <button class="linkbottom">
                <i class="fas fa-arrow-circle-left"></i>
            </button>
        </a>
    </div>
    <form method="post" action="{{ url('/update') }}" class="p-3" style="height:85vh; overflow:scroll;">
        {{ csrf_field() }}

        <input type="hidden" name="id" value="{{ $project->id }}">
        <div class="row create__input--basic">
            <div class="col-4">
                <div class="require-box">
                    <h4>必須項目</h4>
                    <label class="col-12" for="title">title</label>
                    <input class="col-12" type="text" name="title" id="title" value="{{ $project->title }}" required>

                    <label class="col-12" for="clients">clients</label>
                    <input class="col-12" type="text" name="clients" id="clients" value="{{ $project->clients }}">

                    <label class="col-12" for="categories">categories</label>
                    <input class="col-12" type="text" name="categories" id="categories"
                        value="{{ $project->categories }}" required>
                </div>
            </div>
            <div class="col-4">
                <div class="require-box">
                    <h4>募集概要</h4>

                    <label class="col-12" for="work_period">work_period</labe" value="">
                        <input class="col-12" type="text" name="work_period" id="work_period"
                            value="{{ $project->work_period }}">

                        <label class="col-12" for="work_time">work_time</label>
                        <input class="col-12" type="text" name="work_time" id="work_time"
                            value="{{ $project->work_time }}">

                        <label class="col-12" for="work_place">work_place</label>
                        <input class="col-12" type="text" name="work_place" id="work_place"
                            value="{{ $project->work_place }}">

                        <label class="col-12" for="salary">salary</label>
                        <input class="col-12" type="text" name="salary" id="salary" value="{{ $project->salary }}">

                        <label class="col-12" for="working_rate">working_rate</label>
                        <input class="col-12" type="number" name="working_rate" id="working_rate"
                            value="{{ $project->working_rate }}">

                        <label class="col-12" for="recruitment_numbers">recruitment_numbers</label>
                        <input class="col-12" type="number" name="recruitment_numbers" id="recruitment_numbers"
                            value="{{ $project->recruitment_numbers }}">
                </div>
            </div>

            <div class="col-4"">
                <div class=" require-box">
                <h4>募集条件</h4>

                <label class="col-12" for="require_skill">require_skill</label>
                <input class="col-12" type="text" name="require_skill" id="require_skill"
                    value="{{ $project->require_skill }}">

                <label class="col-12" for="desire_skill">desire_skill</label>
                <input class="col-12" type="text" name="desire_skill" id="desire_skill"
                    value="{{ $project->desire_skill }}">

                <label class="col-12" for="end_clients">end_clients</label>
                <input class="col-12" type="text" name="end_clients" id="end_clients"
                    value="{{ $project->end_clients }}">

                <label class="col-12" for="work_experiens">work_experiens</label>
                <input class="col-12" type="text" name="work_experiens" id="work_experiens"
                    value="{{ $project->work_experiens }}">

                <label class="col-12" for="business_contents">business_contents</label>
                <input class="col-12" type="text" name="business_contents" id="business_contents"
                    value="{{ $project->business_contents }}">

                <label class="col-12" for="free_contents">free_contents</label>
                <input class="col-12" type="text" name="free_contents" id="free_contents"
                    value="{{ $project->free_contents }}">
            </div>
        </div>
        <div class="d-flex pr-3 col-12 justify-content-center my-3" style="height:10%">
            <button type="submit" class="linkbottom" style="width:auto">NEW PROJECT</button>
        </div>
    </form>
</div>

@endsection

@section('script')

@endsection
