@extends('layout.app')

@section('title', 'Project Read')

@section('head')
@endsection

@section('content')
<div class="admin_box mt-3" style="background-color:white; overflow:scroll;">
    <div class="text-center">
        <p>TITLE<span style="font-size:40px; margin:0 15px;">{{ $project->title }}</span></p>
        <div class="d-flex buttom_box justify-content-center">
            <div>
                <a href="/project/admin" method="get">
                    <button class="linkbottom">
                        <i class="fas fa-arrow-circle-left"></i>
                    </button>
                </a>
            </div>
            <div>
                <form action="{{ url('/project/edit', $project->id)}}" method="get">
                    {{ csrf_field() }}
                    <button class="linkbottom"><i class="fas fa-edit"></i></button>
                </form>
            </div>
            <div>
                <form action="/project/create" method="get">
                    {{ csrf_field() }}
                    <button class="linkbottom"><i class="far fa-sticky-note"></i></button>
                </form>
            </div>
            <div>
                <form action="{{url('/project/delete',$project->id)}}" method="post">
                    {{ csrf_field() }}
                    <button class="linkbottom"><i class="fas fa-trash-alt"></i></button>
                </form>
            </div>
        </div>
    </div>
    <div class="p-3" style="height:85vh; overflow:scroll;">
        {{-- @csrf --}}
        {{ csrf_field() }}

        <div class="row create__input--basic">
            <div class="col-6">
                <div class="require-box">
                    <h4>募集概要</h4>
                    <label class="col-12" for="title">title</label>
                    <p class="col-12" type="text" name="title" id="title" required>{{ $project->title}}</p>

                    <label class="col-12" for="clients">clients</label>
                    <p class="col-12" type="text" name="clients" id="clients">{{ $project->clients }}</p>

                    <label class="col-12" for="categories">categories</label>
                    <p class="col-12" type="text" name="categories" id="categories" required>{{ $project->categories }}
                    </p>

                    <label class="col-12" for="work_period">work_period</label>
                    <p class="col-12" type="text" name="work_period" id="work_period">{{ $project->work_period }}</p>

                    <label class="col-12" for="work_time">work_time</label>
                    <p class="col-12" type="text" name="work_time" id="work_time">{{ $project->work_time }}</p>

                    <label class="col-12" for="work_place">work_place</label>
                    <p class="col-12" type="text" name="work_place" id="work_place">{{ $project->work_place }}</p>

                    <label class="col-12" for="salary">salary</label>
                    <p class="col-12" type="text" name="salary" id="salary">{{ $project->salary }}</p>

                    <label class="col-12" for="working_rate">working_rate</label>
                    <p class="col-12" type="number" name="working_rate" id="working_rate">{{ $project->working_rate }}
                    </p>

                    <label class="col-12" for="recruitment_numbers">recruitment_numbers</label>
                    <p class="col-12" type="number" name="recruitment_numbers" id="recruitment_numbers">
                        {{ $project->recruitment_numbers }}</p>
                </div>
            </div>

            <div class="col-6">
                <div class="require-box">
                    <h4>募集条件</h4>

                    <label class="col-12" for="require_skill">require_skill</label>
                    <p class="col-12" type="text" name="require_skill" id="require_skill">{{ $project->require_skill }}
                    </p>

                    <label class="col-12" for="desire_skill">desire_skill</label>
                    <p class="col-12" type="text" name="desire_skill" id="desire_skill">{{ $project->desire_skill }}</p>

                    <label class="col-12" for="end_clients">end_clients</label>
                    <p class="col-12" type="text" name="end_clients" id="end_clients">{{ $project->end_clients }}</p>

                    <label class="col-12" for="work_experiens">work_experiens</label>
                    <p class="col-12" type="text" name="work_experiens" id="work_experiens">
                        {{ $project->work_experiens }}</p>

                    <label class="col-12" for="business_contents">business_contents</label>
                    <p class="col-12" type="text" name="business_contents" id="business_contents">
                        {{ $project->business_contents }}</p>

                    <label class="col-12" for="free_contents">free_contents</label>
                    <p class="col-12" type="text" name="free_contents" id="free_contents">{{ $project->free_contents }}
                    </p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection
