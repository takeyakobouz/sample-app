@extends('layout.app')

@section('title', 'Project Create')

@section('head')
@endsection

@section('content')
<div class="admin_box mt-3" style="background-color:white">
    <div class="text-center">
        <h1>create</h1>
        <a href="/project/admin" method="get">
            <button class="linkbottom">
                <i class="fas fa-arrow-circle-left"></i>
            </button>
        </a>
    </div>
    <form method="post" action="{{ url('/posts') }}" class="p-3" style="height:85vh; overflow:scroll;">
        {{-- @csrf --}}
        {{ csrf_field() }}

        <div class="row create__input--basic">
            <div class="col-4">
                <div class="require-box">
                    <h4>必須項目</h4>
                    <label class="col-12" for="title">title</label>
                    <input class="col-12" type="text" name="title" id="title" required>

                    <label class="col-12" for="clients">clients</label>
                    <input class="col-12" type="text" name="clients" id="clients">

                    <label class="col-12" for="categories">categories</label>
                    <input class="col-12" type="text" name="categories" id="categories" required>
                </div>
            </div>
            <div class="col-4">
                <div class="require-box">
                    <h4>募集概要</h4>

                    <label class="col-12" for="work_period">work_period</label>
                    <input class="col-12" type="text" name="work_period" id="work_period">

                    <label class="col-12" for="work_time">work_time</label>
                    <input class="col-12" type="text" name="work_time" id="work_time">

                    <label class="col-12" for="work_place">work_place</label>
                    <input class="col-12" type="text" name="work_place" id="work_place">

                    <label class="col-12" for="salary">salary</label>
                    <input class="col-12" type="text" name="salary" id="salary">

                    <label class="col-12" for="working_rate">working_rate</label>
                    <input class="col-12" type="number" name="working_rate" id="working_rate">

                    <label class="col-12" for="recruitment_numbers">recruitment_numbers</label>
                    <input class="col-12" type="number" name="recruitment_numbers" id="recruitment_numbers">
                </div>
            </div>

            <div class="col-4">
                <div class="require-box">
                    <h4>募集条件</h4>

                    <label class="col-12" for="require_skill">require_skill</label>
                    <input class="col-12" type="text" name="require_skill" id="require_skill">

                    <label class="col-12" for="desire_skill">desire_skill</label>
                    <input class="col-12" type="text" name="desire_skill" id="desire_skill">

                    <label class="col-12" for="end_clients">end_clients</label>
                    <input class="col-12" type="text" name="end_clients" id="end_clients">

                    <label class="col-12" for="work_experiens">work_experiens</label>
                    <input class="col-12" type="text" name="work_experiens" id="work_experiens">

                    <label class="col-12" for="business_contents">business_contents</label>
                    <input class="col-12" type="text" name="business_contents" id="business_contents">
                    {{-- <textarea name="" id="" cols="30" rows="10" wrap="hard"></textarea> --}}

                    <label class="col-12" for="free_contents">free_contents</label>
                    <input class="col-12" type="text" name="free_contents" id="free_contents">


                </div>
            </div>
        </div>
        <div class="d-flex justify-content-center my-3" style="height:10%">
            <button type="submit" class="linkbottom" style="width:auto">NEW PROJECT</button>
        </div>
    </form>
</div>
</div>
@endsection

@section('script')
@endsection
