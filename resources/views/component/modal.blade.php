<div class="d-flex justify-content-around p-3">
    {{-- オリジナルもーだる --}}
    <div class="sample__modal col-4">
        {{-- オリジナルもーだる__ボタン部分 --}}
        <div id="open" style="background-image:url('/img/sample4.jpg')">詳細を見る </div>
        <div id="mask" class="hidden"></div>
        {{-- モーダル部分 --}}
        <section id="modal" class="hidden">
            <p>He said that that that that that boy used in the sentence was
                wrong.<br>あの少年がその文で使ったあのthatは間違っていたと彼は言った。</p>
            <div id="close">
                閉じる
            </div>
        </section>
    </div>
    {{-- マイクロモーダルボタン部分 --}}
    <div class="col-4">
        <button data-micromodal-trigger="modal-1" class="micro-modal__config"
            style="color:white;background-image:url('/img/sample3.jpg');height:auto;width:200px;padding:12px;">micromodalモーダル</button>
    </div>

    <div class="col-4">
        <ul class="slide d-flex justify-content-around">
            <li class="slide1">
                <a href="/img/sample1.jpg" rel="shadowbox[group]" title="タイトル">
                    <img class="img-sizing" src="/img/sample1.jpg" class="wid200" />
                </a>
            </li>
            <li class="slide1">
                <a href="/img/sample3.jpg" rel="shadowbox[group]" title="タイトル">
                    <img class="img-sizing" src="/img/sample3.jpg" class="wid200" />
                </a>
            </li>
            <li class="slide1">
                <a href="/img/sample4.jpg" rel="shadowbox[group]" title="タイトル">
                    <img class="img-sizing" src="/img/sample4.jpg" class="wid200" />
                </a>
            </li>
        </ul>
    </div>
</div>
