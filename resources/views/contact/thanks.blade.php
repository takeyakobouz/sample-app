@extends('layout.app')

@section('title', 'Contact Thanks')

@section('head')
@endsection

@section('content')
<div class="container special-contain text-center">
    <div class="Callendar-main">
    </div>
    <h1 class="col-12 text-top-mm">
        お問い合わせ
    </h1>
</div>
<div class="row justify-content-center">
    <div class="row justify-content-center text-center">
        <p class="my-0 text-center d-inline-block col-11">この度はお問い合わせ頂きまして
            <span class="d-block d-sm-inline">
                ありがとうごさいます。
            </span>
        </p>
        <p class="mb-0 text-center d-inline-block col-11">担当の者よりご連絡致します。</p>
    </div>
</div>
@endsection

@section('script')

@endsection
