@extends('layout.app')

@section('title', 'Contact')

@section('head')
@endsection

@section('content')
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-57757680-1', 'auto');
    ga('require', 'displayfeatures');
    ga('send', 'pageview');

</script>
<script type="text/babel" data-presets="es2015,stage-2">

    class Validator {

		constructor(){
			this.rules = new Map([
				["name", "required"],
				["email", "required|email"],
				["message", "required"],
				["agree", "agree"],
			])

			this.labels = {
				name: "お名前",
				email: "メールアドレス",
				message: "お問い合わせ内容",
				agree: "個人情報保護方針",
			}

			this.messages = {
				required: ":labelは必須項目です。",
				email: "正しい:labelを入力してください。",
				agree: ":labelをご確認の上、送信して下さい",
			}
			this.errors = new Map()
		}

		run(){
			this.errors = new Map()
			const errorMessage = document.querySelectorAll('.error-message')
			Array.from(errorMessage).forEach(el => el.innerHTML = '')

			this.rules.forEach((value, key) => {
				value.split('|').forEach(rule => this[rule](key))
			})

			if (this.errors.size) {
				this.errors.forEach((error, key) => {
					const el = document.getElementById(`${key}-error`)
					el.innerHTML = error
				})
				return false
			}
			return true
		}

		required(key){
			const el = document.querySelector(`[name="${key}"]`)
			if (!el.value) {
				this.setErrorMessage(key, "required")
			}
		}

		email(key){
			const el = document.querySelector(`[name="${key}"]`)
			if (!el.value.match(/^[A-Za-z\._\-[0-9]*[@][A-Za-z0-9-]*[\.][a-z]{2,4}$/)){
				this.setErrorMessage(key, "email")
			}
		}

		agree(key){
			const el = document.querySelector(`[name="${key}"]`)
			if(!el.checked){
				this.setErrorMessage(key, "agree")
			}
		}

		setErrorMessage(key, rule) {
			if(!this.errors.get(key)){
				this.errors.set(key, this.messages[rule].replace(/:label/, this.labels[key]))
			}
		}
	}

	(() => {
		const validator = new Validator()
		const submit = document.querySelector('.submit-btn')
		submit.addEventListener('click', () => {
			const valid = validator.run()
			if(valid){d
				$('#form').submit();
			}
		}, false)
	})()
</script>
<div class="m-5">
    <div id="main-content">
        <div class="">
            <h2 class="" style="border-bottom:black 2px solid">お問い合わせ</h2>
            <form class="p-5" style="background-color:#EFEFEF;" method="post" id="form" action="./contact">
                <div class="" style="border-bottom:#C7C7C7 1px dotted">
                    <p>パックスクリエイションへのお問い合わせは、下記のフォームをご記入ください。</p>
                    <p>メールにて回答させていだだきます。</p>
                    <p style="color: red;">*は必須項目です。</p>
                </div>
                <div class="">
                    <div class="firstname">
                        <p class=""><span>*</span>お名前</p>
                        <input style="min-wight:150px;" type="text" name="name" id="contact-name">
                    </div>
                    <div>
                        <span class="error-message" id="name-error" style="color:red"></span>
                    </div>
                    <div class=" email">
                        <span class=""><span>*</span>メールアドレス</span>
                        <input type="text" name="email" id="contact-email">
                    </div>
                    <div>
                        <span class="error-message" id="email-error" style="color:red"></span>
                    </div>
                    <div class="content">
                        <span class=""><span>*</span>お問い合わせ内容</span>
                        <textarea rows="7" name="message" id="contact-message"></textarea>
                    </div>
                    <div>
                        <span class="error-message" id="message-error" style="color:red"></span>
                    </div>

                    <div>
                        <p class="text-center">「<a href="contact/privacyPolicy#about_privacy" methods="" target="_blank"
                                style="color:red;">個人情報の取扱いについて</a>」ご同意いただいた上で、送信ボタンをクリックしてお問い合わせください。</p>
                    </div>
                    <div>
                        <p class="text-center">
                            <input name="agree" type="checkbox" value="1" id="contact-agree">
                            <input name="agree" type="hidden" value="0">
                            <label for="contact-agree">個人情報の取扱いについて同意する</label>
                        </p>
                    </div>
                    <div>
                        <span class="error-message" id="agree-error" style="color:red"></span>
                    </div>
                    <input class="submit-btn" value="送 信" type="button" />
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
