@extends('layout.app')

@section('title', 'Remaindar')

@section('head')
<script src="{{ asset('/css/modal.js') }}"></script>
<script src="https://unpkg.com/micromodal/dist/micromodal.min.js"></script>
@endsection

@section('content')
<div>
    <code>codeタグ</code>
    <p v-if="isBitween('2019-07-19 14:00:00','2019-07-19 15:00:00','hour')">print</p>
    <div id="parent" class="d-flex col-12 justify-content-between" style="height:100px;"></div>
    <div id="app" class="m-4 fuu">
        <a href="/welcome">
            <h1 v-bind:title="todosMessage">@{{ todosMessage }}</h1>
        </a>
        <div id="buttomGood">
            <v-buttom-component message='success'></v-buttom-component>
        </div>

        <button class="btn btn-danger" @click="purge">Purge</button>
        <span class="info">(@{{ remaining.length }}/@{{ todos.length }})</span>
        <ul v-if="todos.length">
            <li v-for="(todo, index) in todos">
                <input type="checkbox" v-model="todo.isDone">
                <span :class="{done:todo.isDone}">@{{ todo.title }}</span>
                <span @click="deleteItem(index)" class="deleteItem">[x]</span>
            </li>
        </ul>
        <ul v-else>
            <li>Let NEW !</li>
        </ul>

        <form clas="home-todo__form" @submit.prevent="addItem">
            <input type="text" v-model="newItem">
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>

    <div class="modal-html__box .area">
        <!-- [1] -->
        <div class="modal micromodal-slide" id="modal-1" aria-hidden="true">
            <!-- [2] -->
            <div class="modal__overlay" tabindex="-1" data-micromodal-close>
                <!-- [3] -->
                <div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
                    <header class="modal__header">
                        <h2 class="modal__title" id="modal-1-title">
                            Micromodal
                        </h2>
                        <!-- [4] -->
                        <button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
                    </header>
                    <main class="modal__content" id="modal-1-content">
                        <p>
                            <code>tab</code>で選択する切り替え
                            <code>esc</code>で終了
                        </p>
                    </main>
                    <footer class="modal__footer">
                        <button class="modal__btn modal__btn-primary">Continue</button>
                        <button class="modal__btn" data-micromodal-close
                            aria-label="Close this dialog window">Close</button>
                    </footer>
                </div>
            </div>
        </div>
    </div>


    <div class="d-flex justify-content-around p-3">
        {{-- オリジナルもーだる --}}
        <div class="sample__modal col-4">
            {{-- オリジナルもーだる__ボタン部分 --}}
            <div id="open" style="background-image:url('/img/sample4.jpg')">詳細を見る </div>
            <div id="mask" class="hidden"></div>
            {{-- モーダル部分 --}}
            <section id="modal" class="hidden">
                <p>He said that that that that that boy used in the sentence was
                    wrong.<br>あの少年がその文で使ったあのthatは間違っていたと彼は言った。</p>
                <div id="close">
                    閉じる
                </div>
            </section>
        </div>
        {{-- マイクロモーダルボタン部分 --}}
        <div class="col-4">
            <button data-micromodal-trigger="modal-1" class="micro-modal__config"
                style="color:white;background-image:url('/img/sample3.jpg');height:auto;width:200px;padding:12px;">micromodalモーダル</button>
        </div>

        <div class="col-4">
            <ul class="slide d-flex justify-content-around">
                <li class="slide1">
                    <a href="/img/sample1.jpg" rel="shadowbox[group]" title="タイトル">
                        <img class="img-sizing" src="/img/sample1.jpg" class="wid200" />
                    </a>
                </li>
                <li class="slide1">
                    <a href="/img/sample3.jpg" rel="shadowbox[group]" title="タイトル">
                        <img class="img-sizing" src="/img/sample3.jpg" class="wid200" />
                    </a>
                </li>
                <li class="slide1">
                    <a href="/img/sample4.jpg" rel="shadowbox[group]" title="タイトル">
                        <img class="img-sizing" src="/img/sample4.jpg" class="wid200" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
    @endsection

    @section('script')
    <script src="{{ asset('/js/main.js') }}"></script>

    <script src="{{ asset('/js/modal.js') }}"></script>
    <script>
        MicroModal.init();

    </script>

    <script src="{{ asset('/js/todos.js') }}"></script>
    <script src="{{ asset('/js/goodSign.js') }}"></script>
    @endsection
