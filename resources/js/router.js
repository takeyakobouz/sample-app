import Vue from 'vue'
import VueRouter from 'vue-router'

// 読み込むvueの追加
import PhotoList from './pages/PhotoList.vue'
import Login from './pages/Login.vue'

Vue.use(VueRouter)

// routeの置き場所
const routes = [{
    path: '/',
    component: PhotoList
}, {
    path: '/login',
    component: Login
}]
const router = new VueRouter({
    // mode: 'history',
    routes
})
export default router
