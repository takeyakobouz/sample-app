<?php

namespace App\Http\Controllers;

use Illuminate\Support\Carbon;

class CallendarController extends Controller
{
    public $prev;
    public $next;
    public $yearMonth;
    public $nowDate;
    public $callendar = [];

    public function __construct()
    {

    }
    public function prev()
    {
        $getDate = (new Carbon())->subMonth();
        return $this->creatCallendar($getDate->toDateTimeString());
    }

    public function next()
    {
        $getDate = (new Carbon())->addMonth();
        return $this->creatCallendar($getDate->toDateTimeString());

    }

    public function index()
    {
        $dt = Carbon::parse('2009-04-04');
        $dt = new Carbon();

        $getDate = $dt->toDateTimeString();
        return $this->creatCallendar($getDate);
    }

    public function creatCallendar($getDate)
    {
        $nowDate = Carbon::create($getDate);
        $tail = $this->_getTail($nowDate);
        $body = $this->_getBody($nowDate);
        $head = $this->_getHead($nowDate);
        $nowdate = $this->callendar;
        //----------------------//

        $year = $nowDate->year;
        $month = $nowDate->month;
        $monthEng = $nowDate->format('F');
        $week = ['Sunday', 'Monday', 'Tuesday', 'Wendnesday', 'Thursday', 'Friday', 'Sarturday'];
        return view('/callendar/index', compact('nowdate', 'week', 'month', 'year', 'monthEng'));
    }

    private function _getTail($nowDate)
    {
        $lastDayOfPrevMonth = Carbon::create('last day of' . $nowDate . '-1 month');
        while ($lastDayOfPrevMonth->copy()->dayOfWeek < 6) {
            array_unshift($this->callendar, $lastDayOfPrevMonth->copy());
            $lastDayOfPrevMonth->subDay();
        }
        return $this->callendar;
    }

    //今月は置いておく
    private function _getBody($nowDate)
    {
        $lastDate = $nowDate->copy()->daysInMonth;
        $day = Carbon::create('first day of ' . $nowDate);
        for ($i = 0; $i < $lastDate; $i++) {
            array_push($this->callendar, $day->copy());
            $day->addday();
        }
        return $this->callendar;
    }

    private function _getHead($nowDate)
    {
        $firstDayOfNextMonth = $nowDate->copy()->addMonth()->firstOfMonth();
        for ($i = 0; $firstDayOfNextMonth->copy()->dayOfWeek != 0; $i++) {
            array_push($this->callendar, $firstDayOfNextMonth->copy());
            $firstDayOfNextMonth->addDay();

        }
        return $this->callendar;
        // }
    }

    public function jsCallendar()
    {
        return view('/callendar/jsCallendar');
    }
}
