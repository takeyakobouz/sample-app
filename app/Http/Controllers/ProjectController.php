<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
/*******************
 *create
 ********************/
    public function Create()
    {
        return view('/project/create');
    }
    public function Store(Request $request)
    {
        $project = new Project();
        $project->fill($request->all())->save();
        return redirect('/project/admin');
    }
/*******************
 *read
 ********************/
    public function Read($id)
    {
        $project = Project::findOrFail($id);
        return view('/project/read')->with('project', $project);
    }
    public function Admin()
    {
        $projects = Project::orderBy('updated_at', 'desc')->get();
        return view('/project/admin')->with('projects', $projects);

    }
/*******************
 *update
 ********************/
    public function Edit($id)
    {
        $project = Project::findOrFail($id);
        return view('/project/edit')->with('project', $project);
    }
    public function Update(Request $request)
    {
        // dd($request);
        $project = Project::find($request->id);
        // dd($request->all());
        // dd($project);

        $project->fill($request->all())->save();

        return redirect('/project/admin');
    }
/*******************
 *delete
 ********************/
    public function delete($id)
    {
        Project::destroy($id);
        return redirect('/project/admin');
    }
}
