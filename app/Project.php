<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'title',
        'clients',
        'categories',
        'work_period',
        'work_time',
        'work_place',
        'salary',
        'require_skill',
        'desire_skill',
        'working_rate',
        'recruitment_numbers',
        'end_clients',
        'work_experiens',
        'business_contents',
        'free_contents',
    ];
}
