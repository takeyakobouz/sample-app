##config
laravel 5.8
php 5.7
mysql Ver 14.14 Distrib 5.7.26, for osx10.14 (x86_64) using EditLine wrapper
node v10.16.0
##setting
`composer install`
`npm install`
`cp -p .env.example .env`
`php artisan key:generate`
`npm install -D vue-router`
`npm install --save-dev vuex`
`npm install vuex --save`
##DB

`CREATE USER 'admin'@'localhost' IDENTIFIED WITH mysql_native_password BY 'Js_workshare123';`

`GRANT ALL ON js_workshare.* TO admin@localhost;`

CREATE DATABASE `js_workshare`;
use `js_workshare`

CREATE TABLE `projects` (
`id` bigint NOT NULL AUTO_INCREMENT,
`title` varchar(255) NOT NULL,
`clients` varchar(255) DEFAULT NULL,
`categories` varchar(255) NOT NULL,
`work_period` varchar(255) DEFAULT NULL,
`work_time` varchar(255) DEFAULT NULL,
`work_place` varchar(255) DEFAULT NULL,
`salary` varchar(255) DEFAULT NULL,
`require_skill` varchar(255),
`desire_skill` varchar(255),
`working_rate` int DEFAULT NULL,
`recruitment_numbers` int DEFAULT NULL,
`end_clients` varchar(255) DEFAULT NULL,
`work_experiens` varchar(255) DEFAULT NULL,
`business_contents` longtext DEFAULT NULL,
`free_contents` longtext DEFAULT NULL,
`created_at` timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
`updated_at` timestamp DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
);
+---------------------+---------------------+------+-----+---------+----------------+
| Field | Type | Null | Key | Default | Extra |
+---------------------+---------------------+------+-----+---------+----------------+
| id | bigint(20) unsigned | NO | PRI | NULL | auto_increment |
| title | varchar(255) | NO | | NULL | |
| clients | varchar(255) | YES | | NULL | |
| categories | varchar(255) | NO | | NULL | |
| work_period | varchar(255) | YES | | NULL | |
| work_time | varchar(255) | YES | | NULL | |
| work_place | varchar(255) | YES | | NULL | |
| salary | varchar(255) | YES | | NULL | |
| require_skill | varchar(255) | YES | | NULL | |
| desire_skill | varchar(255) | YES | | NULL | |
| working_rate | int(11) | YES | | NULL | |
| recruitment_numbers | int(11) | YES | | NULL | |
| end_clients | varchar(255) | YES | | NULL | |
| work_experiens | varchar(255) | YES | | NULL | |
| business_contents | longtext | YES | | NULL | |
| free_contents | longtext | YES | | NULL | |
| created_at | timestamp | NO | | NULL | |
| updated_at | timestamp | YES | | NULL | |
+---------------------+---------------------+------+-----+---------+----------------+
