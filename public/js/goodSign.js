(function() {
    'use strict';

    var likeComponent = Vue.extend({
        props: {
            message: {
                type: String,
                default: 'Like'
            }
        },
        data: function() {
            return {
                count: 0
            }
        },
        template: '<button class="btn btn-success" @click="countUp">{{ message }} {{ count }}</button>',
        methods: {
            countUp: function() {
                this.count++;
                console.log(count);
                this.$emit('increment');
            }
        }
    });

    var buttomGood = new Vue({
        el: '#buttomGood',
        components: {
            'v-buttom-component': likeComponent
        },
        data: {
            total: 0
        },
        methods: {
            incrementTotal: function() {
                this.total++;
                console.log(total);
            }
        }
    });

})();