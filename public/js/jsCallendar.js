'use strict';

const weeks = ['sun', 'mon', 'tue', 'wed', 'tur', 'fri', 'sut'];
const date = new Date();
const year = date.getFullYear();
const month = date.getMonth() + 1;
const startDate = new Date(year, month - 1, 1);

const endDate = new Date(year, month, 0);
const endDayCount = endDate.getDate();
const startDay = startDate.getDay();

let dayCount = 1;
let calendarHTML = '';

calendarHTML += '<table>';

for (let i = 0; i < weeks.length; i++) {
    calendarHTML += '<td>' + weeks[i] + '</td>';
};

for (let w = 0; w < 6; w++) {
    calendarHTML += '<tr>';

    for (let d = 0; d < 7; d++) {
        if (w == 0 && d < startDay) {
            calendarHTML += '<td></td>';
        } else if (dayCount > endDayCount) {
            calendarHTML += '<td></td>';
        } else {
            calendarHTML += '<td>' + dayCount + '</td>';
            dayCount++;

        }
    }
    calendarHTML += '</tr>';
};
calendarHTML += '</table>';
document.querySelector('#jsCallendar').innerHTML = calendarHTML;