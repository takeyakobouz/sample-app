'use strict';
/*******************
 *js game
 ********************/
const num = 10;
const winner = Math.floor(Math.random() * num); // 0 - 4

for (let i = 1; i < num; i++) {
    const div = document.createElement('div');
    div.classList.add('box');
    div.classList.add('col-1');
    if (i === winner) {
        div.dataset.result = 'win';
    } else {
        div.dataset.result = 'lose';
    }
    div.addEventListener('click', function() {
        if (div.dataset.result === 'win') {
            div.textContent = 'Win!';
            div.classList.add('win');
        } else {
            div.textContent = 'Lose!';
            div.classList.add('lose');
        }
    });
    const parent = document.getElementById('parent');
    parent.appendChild(div);
}