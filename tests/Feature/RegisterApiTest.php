<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RegisterApiTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function should_res_create_user()
    {
        $data = [
            'name' => 'js_workshare user',
            'email' => 'dummy@email.com',
            'password' => 'test1234',
            'password_check' => 'test1234',
        ];
        $response = $this->json('POST', route('register'), $data);

        $user = User::first();
        $this->assertEquals($data['name'], $user->name);

        $response
            ->assertStatus(200)
            ->assertJson(['name' => $user->name]);
    }
}
