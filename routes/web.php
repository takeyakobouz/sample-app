<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('top/index/{any?}', function () {
    return view('top/index');
})->where('any', '.+');

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::get('/privacyPolicy', 'ContactController@privacyPolicy');
    Route::get('/thanks', 'ContactController@thanks');
});

Route::group(['prefix' => 'project'], function () {
    Route::get('/admin', 'ProjectController@admin');
    Route::get('/read/{id}', 'ProjectController@read');
    Route::get('/create', 'ProjectController@create');
    Route::get('/edit/{id}', 'ProjectController@edit');
    Route::post('/delete/{id}', 'ProjectController@delete');
    Route::post('/posts', 'ProjectController@store');
    Route::post('/update', 'ProjectController@update');
});

Route::group(['prefix' => 'callendar'], function () {
    Route::get('/', 'CallendarController@index');
    Route::get('/prev', 'CallendarController@prev');
    Route::get('/next', 'CallendarController@next');
});

Route::group(['prefix' => 'remainder'], function () {
    Route::get('/', 'remainderController@index');
    Route::post('/', function (Request $request) {
        dd($request->all());
        return view('Home');
    });
});

Route::get('/test', function () {
    return view('test');
});
