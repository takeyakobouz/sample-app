<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *マイグレーションで実行したい処理
     * @return void
     */
    public function up()
    {

        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('clients')->nullable();;
            $table->string('categories');
            $table->string('work_period')->nullable();;
            $table->string('work_time')->nullable();;
            $table->string('work_place')->nullable();;
            $table->string('salary')->nullable();;
            $table->string('require_skill')->nullable();;
            $table->string('desire_skill')->nullable();;
            $table->integer('working_rate')->nullable();;
            $table->integer('recruitment_numbers')->nullable();;
            $table->string('end_clients')->nullable();;
            $table->string('work_experiens')->nullable();;
            $table->longText('business_contents')->nullable();;
            $table->longText('free_contents')->nullable();;
            $table->timestamp('created_at');
            $table->timestamp('update_at');
        });
    }

    /**
     * Reverse the migrations.
     *マイグレーションで巻き戻したい処理
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
